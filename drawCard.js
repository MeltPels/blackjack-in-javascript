const cardValues = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];
const suit = ['&heartsuit;', '&spadesuit;', '&diamondsuit;', '&clubsuit;'];

function drawCard(){
    const randomIndex = Math.floor(Math.random() * cardValues.length); // random card value
    const randomSuit = Math.floor(Math.random() * suit.length); // random suit
    const randomCardValue = cardValues[randomIndex] + " " + suit[randomSuit] ;

    return randomCardValue
}