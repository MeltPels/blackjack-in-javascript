
let elButton = document.querySelector("button");
let elResults = document.getElementById("results");
let elDealerCards = document.getElementById("dealerResults");
let elStartButton = document.getElementById("start");
let elContinueGame = document.getElementById("continue");
let elContinueButton = document.getElementById("continueButton");
let elPlayerCardTitle = document.getElementById("playercards")
let elDealerCardTitle = document.getElementById("dealercards")
let currentValue = 0;
let dealerValue = 0;

function startGame() {
    elStartButton.innerHTML = ' ';
    let cardOne = drawCard();
    let cardTwo = drawCard();
    elResults.innerHTML = `<p>Card: ${cardOne} </p><p> Card: ${cardTwo}</p>`;
    currentValue = calculateValue(cardOne) + calculateValue(cardTwo);
    dealerDrawsCard();
    checkIfAlive();
}

function revealCard(){
    let additionalCard = drawCard();
    currentValue = currentValue + calculateValue(additionalCard);
    elResults.innerHTML += `<p>Card: ${additionalCard}</p>`;
    checkIfAlive();

}

function checkIfAlive(){
    if (currentValue <= 20) {
        elContinueGame.textContent = "Current value: " + currentValue + " Would you like to draw another card?";
        elContinueButton.innerHTML = '<button onclick="revealCard()">Draw another card</button> <br> <button onclick="dealersTurn()">Keep hand as is</button>';
    } else if (currentValue === 21) {
        elContinueGame.textContent = "You've got Blackjack!";
        elContinueButton.innerHTML = '<button onclick="dealersTurn()">Dealers turn</button>';
    } else {
        elContinueGame.textContent = "You're out of the game!"
        elContinueButton.innerHTML = '<button onclick="startGame()">Restart the game</button>';
    }
}

function dealerDrawsCard(){
    let dealerOne = drawCard();
    elDealerCards.innerHTML += `<p>Card: ${dealerOne} </p>`;
    dealerValue += calculateValue(dealerOne);

}

function dealersTurn(){
    elPlayerCardTitle.textContent = "Your cards: " + currentValue;
    elDealerCardTitle.textContent = "Dealer's cards: " + dealerValue;
    elContinueButton.innerHTML = '';
    if (dealerValue <= 17){
        dealerDrawsCard();
        dealersTurn();
    } else if (dealerValue > 21){
        elContinueGame.textContent = "Dealer busts, you win!";
        elContinueButton.innerHTML = '<button onclick="startGame()">Restart the game</button>';
    } else if (currentValue > dealerValue){
        elContinueGame.textContent = "Your cards are better than the dealers, you win!";
        elContinueButton.innerHTML = '<button onclick="startGame()">Restart the game</button>';
    } else {
        elContinueGame.textContent = "House wins with " + dealerValue + " points.";
        elContinueButton.innerHTML = '<button onclick="startGame()">Restart the game</button>';
    }
    

}

