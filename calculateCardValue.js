
function calculateValue(a){
    const value = a.split(' ')[0]; // Extract the card rank, excluding the suit
  switch (value) {
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
      return parseInt(value);
    case '10':
    case 'J':
    case 'Q':
    case 'K':
      return 10;
    case 'A':
      return 11;
    default:
      return 0; // Invalid card
      }
}