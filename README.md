# Blackjack Game

This is a simple web-based implementation of the Blackjack game written in JavaScript.

## How to Play

1. Clone the repository:

   ```shell
   git clone https://github.com/your-username/blackjack-game.git
2. Navigate to the project directory:
3. Open the index.html file in a web browser to play the game.

4. Click the "Start the game" button to begin.

5. Two cards will be dealt, and the total value of your hand will be displayed.

6. Choose whether to draw another card or stop.

7. If you choose to draw another card, a new card will be added to your hand, and the updated total value will be displayed.

8. The game will continue until you reach a value of 21 (Blackjack), exceed 21 (Bust), or choose to stop.

9. After the game ends, you can restart the game by clicking the "Restart the game" button.

## Dependencies
This project has no external dependencies. It is a self-contained HTML, CSS, and JavaScript implementation of the Blackjack game.

## License
This project is licensed under the MIT License.